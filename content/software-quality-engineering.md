Title: Software Quality Engineering: Tales From The Trenches
Date: 2018-11-10
Category: Home
Tags: book, qe, quality engineering
Slug: software-quality-engineering-tales-from-the-trenches

<script src="https://gumroad.com/js/gumroad-embed.js"></script>
<div class="gumroad-product-embed" data-gumroad-product-id="software-quality-engineering">
    <a href="https://gumroad.com/l/software-quality-engineering">Buy the book</a>
</div>